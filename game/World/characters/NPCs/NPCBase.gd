extends KinematicBody2D

var lastPos

func _ready():
	if(has_node("Sprite")):
		if($Sprite.has_node("AnimationPlayer")):
			if($Sprite/AnimationPlayer.has_animation("idle")):
				$Sprite/AnimationPlayer.play("idle")
	lastPos = position

func _process(delta):
	var dir = lastPos - position
	var anim = $Sprite/AnimationPlayer
	if anim.has_animation("idle") and not dir and anim.current_animation != "idle":
		anim.play("idle")
	elif anim.has_animation("walk") and dir and anim.current_animation != "walk":
		anim.play("walk")
	if dir.x != 0:
		$Sprite.flip_h = (dir.x > 0)
	lastPos = position

func interact(item):
	if $Interact.has_method("interact"):
		return $Interact.interact(item)
	$Reaction.react("quest")
	return false
