extends StaticBody2D

const SFX = preload("res://World/SuccessSFX.tscn")

var done = false

func interact(item):
	if not done and item != null and item.extern_name == "coins":
		done = true
		var bag = get_node("/root/Main/HUD/Bag")
		bag.remove_item("coins")
		yield(bag, "item_removed")
		bag.add_item("bread")
		self.queue_free()
		get_parent().add_child(SFX.instance())