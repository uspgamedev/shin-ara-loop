extends Control

var fixed = false

func _ready():
	$Timer.start()
	pass

func fix():
	fixed = true

func _on_Timer_timeout():
	if $Sprite.frame == 9:
		$Sprite.frame = 10
		$FinalSFX.play()
		get_parent().end_day()
	else:
		$Sprite.frame = ($Sprite.frame+1)%12
		$Timer.start()
		if $Sprite.frame == 6:
			$QuarterClangSFX.play()
		else:
			$TickSFX.play()

func set_slided_frame(num,tick=false):
	$Sprite.frame = (int(num) + 10)%12
	if tick:
		$TickSFX.play()

func get_slided_frame():
	return ($Sprite.frame + 2)%12
