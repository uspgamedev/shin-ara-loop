extends "res://World/characters/NPCs/NPCBase.gd"

export (NodePath) var destination

onready var tween = get_node("Tween")

const ANIMATION_TIME = 7

func flyAway():
	tween.interpolate_property(self, "position", position, get_node(destination).position,
							ANIMATION_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	queue_free()