extends Container

const ITEM = preload("res://HUD/Item.tscn")

var selected = 0

signal item_removed

func _ready():
	var count = 0
	for child in $Items.get_children():
		child.rect_position.y = count*80
		count += 1

func _process(delta):
	for i in range(3):
		var name = "Slot" + var2str(i + 1)
		var slot = get_node(name)
		if i == selected:
			slot.select()
		else:
			slot.unselect()

func add_item(name):
	var num_items = $Items.get_child_count()
	if num_items < 3:
		var texture = load("res://World/Items/" + name + ".png")
		var item  = ITEM.instance()
		item.extern_name = name
		item.texture = texture
		item.rect_position.y = num_items*80
		$Items.add_child(item)
		return true
	return false

func remove_item(name):
	var count = 0
	var removed = false
	print(name)
	for child in $Items.get_children():
		if child.extern_name != name or removed:
			child.rect_position.y = count*80
			count += 1
		elif not removed:
			print("Removed " + child.extern_name)
			$Items.remove_child(child)
			call_deferred("_finish_removal", child)
			removed = true

func _finish_removal(child):
	child.free()
	emit_signal("item_removed")

func next_item():
	self.selected = (self.selected + 1) % 3

func has_item(name):
	for child in $Items.get_children():
		if child.extern_name == name:
			return true
	return false

func selected_item():
	if $Items.get_child_count() < selected + 1:
		return null
	return $Items.get_children()[selected]