extends StaticBody2D

const SFX = preload("res://World/GotItemSFX.tscn")

func interact(item):
	if get_node("/root/Main/HUD").add_item(self.name):
		self.queue_free()
		get_parent().add_child(SFX.instance())
