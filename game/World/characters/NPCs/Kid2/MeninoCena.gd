extends "res://World/characters/NPCs/NPCBase.gd"

export (NodePath) var homeStraight
export (NodePath) var swingPath1
export (NodePath) var swingPath2

enum state{
	waitingBakery,
	walking,
	waitingSwing,
	atHome
}

const ANIMATION_TIME = 4.5
onready var tween = get_node("Tween")

var currState = waitingBakery
var isAwayFromSister = false

func _on_Timer_timeout():
	if(currState == state.waitingBakery):
		tween.interpolate_property(self, "position", position, get_node(swingPath1).position,
						ANIMATION_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		tween.start()
		currState = walking;
		yield(tween, "tween_completed")
		currState = waitingSwing

func walkAway():
	match(currState):
		state.waitingBakery:
			tween.interpolate_property(self, "position", position, get_node(homeStraight).position,
							ANIMATION_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT)
			tween.start()
			currState = walking
			yield(tween, "tween_completed")
			currState = atHome
		state.waitingSwing:
			tween.interpolate_property(self, "position", position, get_node(homeStraight).position,
							ANIMATION_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT)
			tween.start()
			currState = walking
		_:
			pass
			
			
func fixGear():
	if(!isAwayFromSister and currState == state.atHome):
		var bag = get_node("/root/Main/HUD/Bag")
		bag.remove_item("brokenGear")
		yield(bag, "item_removed")
		bag.add_item("fixedGear")
	
func _on_Area2D_body_entered(body):
	if(body.name == "Menina"):
		isAwayFromSister = false
