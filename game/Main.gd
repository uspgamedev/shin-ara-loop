extends YSort

const world = preload("res://World/World.tscn")

export(String) var slot1 = "coins"
export(String) var slot2 = null
export(String) var slot3 = null

var current_world

func _ready():
	current_world = get_node("World")
	if slot1 != null: $HUD.add_item(slot1)
	if slot2 != null: $HUD.add_item(slot2)
	if slot3 != null: $HUD.add_item(slot3)

func _on_HUD_beginClockAnim():
	var next_world = world.instance()
	$HUD/Underworld.visible = true
	get_tree().paused = true
	var tween = Tween.new()
	tween.interpolate_property($HUD/Underworld, 'modulate', Color(1, 1, 1, 0), Color(1, 1, 1, 1), 4, Tween.EASE_IN, Tween.EASE_OUT)
	tween.start()
	$HUD.add_child(tween)
	yield(get_tree().create_timer(4), 'timeout')
	current_world.queue_free()
	yield(get_tree().create_timer(0.1), 'timeout')
	add_child(next_world)
	current_world = next_world

func _on_HUD_endClockAnim():
	get_tree().paused = false
	$HUD/Underworld.modulate = Color(1, 1, 1, 0)
	$HUD/Underworld.visible = false
	$BGM.volume_db = 0
	$BGM.seek(0)
	$BGM.play()
