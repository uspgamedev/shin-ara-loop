extends Control
const SFX = preload("res://World/SuccessSFX.tscn")
func interact(unused):
	get_node("/root/Main/HUD").add_item("coins")
	get_node("../Reaction").react("bread")
	get_node("..").get_parent().add_child(SFX.instance())
	return false