extends "res://World/characters/NPCs/NPCBase.gd"

var withRope = false
var awayFromCrow = false

func enableCollision(body):
	if(body.name == "Crow"):
		print("Saiu do ninho")
		awayFromCrow = true
		$Sprite.show()
	elif(body.name == "Tree"):
		if(awayFromCrow):
			withRope = true
			$CollisionShape2D.disabled = false