extends "res://World/characters/NPCs/NPCBase.gd"

export (NodePath) var dogPos
export (NodePath) var pathToHouse1
export (NodePath) var pathToHouse2

const ANIMATION_TIME = 4.5
const ANIMATION_TIME_2 = 5.5
onready var tween = get_node("Tween")

var waited = false
	
func _physics_process(delta):
	if(waited):
		tween.interpolate_property(self, "position", position, get_node(dogPos).position,
								ANIMATION_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		tween.start()

		set_physics_process(false)
		
func _on_Timer_timeout():
	waited = true


func _on_Area2D_body_entered(body):
	if(body.name == "Dog"):
		$PettingDog.start()


func _on_PettingDog_timeout():
	tween.interpolate_property(self, "position", position, get_node(pathToHouse1).position,
							ANIMATION_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	tween.interpolate_property(self, "position", position, get_node(pathToHouse2).position,
							ANIMATION_TIME_2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()

