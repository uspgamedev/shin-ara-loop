extends Path2D

export (bool) var cyclic
export (float) var speed = 1

onready var points = get_curve().get_baked_points()

func _process(delta):
	update()