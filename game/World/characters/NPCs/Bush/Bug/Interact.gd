extends Control

const SFX = preload("res://World/SuccessSFX.tscn")

func interact(item):
	get_node("/root/Main/HUD").add_item("bug")
	var par = get_node("..")
	par.get_parent().add_child(SFX.instance())
	par.get_node("..").get_node("Destructor").queue_free()
	par.get_node("..").get_node("Generator").queue_free()
	par.queue_free()
	return false