extends Control
const SFX = preload("res://World/SuccessSFX.tscn")
func interact(item):
	if item != null:
		if item.extern_name == "bread":
			get_node("/root/Main/HUD").remove_item("bread")
			get_node("../Reaction").react("happy")
			get_parent().add_child(preload("res://World/SuccessSFX.tscn").instance())
			return get_node("..").walkAway()
			get_node("..").get_parent().add_child(SFX.instance())
		elif(item.extern_name == "brokenGear"):
			get_parent().add_child(preload("res://World/SuccessSFX.tscn").instance())
			return get_node("..").fixGear()
			get_node("..").get_parent().add_child(SFX.instance())
	get_node("../Reaction").react("quest")
	return false
