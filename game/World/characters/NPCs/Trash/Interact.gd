extends Control
const SFX = preload("res://World/SuccessSFX.tscn")
var hasBone = true

func interact(unused):
	if(hasBone):
		get_node("/root/Main/HUD").add_item("bone")
		get_node("..").get_parent().add_child(SFX.instance())
		hasBone = false
		get_node("..").get_node("Sprite").hide()
		get_node("..").get_node("Empty").show()
	return false