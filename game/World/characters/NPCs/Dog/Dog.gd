extends "res://World/characters/NPCs/NPCBase.gd"

export (NodePath) var destination

const ANIMATION_TIME = 5.5
onready var tween = get_node("Tween")

var waited = false
var isFed = false

func _physics_process(delta):
	if(waited):
		if(!isFed):
			tween.interpolate_property(self, "position", position, get_node(destination).position,
								ANIMATION_TIME, Tween.TRANS_QUAD, Tween.EASE_OUT)
			tween.start()
			
		set_physics_process(false)
		
func _on_Timer_timeout():
	waited = true
