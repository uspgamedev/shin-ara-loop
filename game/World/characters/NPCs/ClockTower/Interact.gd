extends Control
const SFX = preload("res://World/SuccessSFX.tscn")
var available = false
var done = false

func open():
	self.available = true
	get_parent().get_node("OpenDoor").visible = true
	get_parent().get_node("DoorSFX").play()

func interact(unused):
	var bag = get_tree().get_root().get_node("Main/HUD/Bag")
	if available and not done and bag.has_item("fixedGear") and bag.has_item("brokenNut") and bag.has_item("brokenNail"):
		done = true
		bag.remove_item("fixedGear")
		bag.remove_item("brokenNut")
		bag.remove_item("brokenNail")
		get_tree().get_root().get_node("Main/HUD/Clock").fix()
		get_parent().get_node("SuccessSFX").play()