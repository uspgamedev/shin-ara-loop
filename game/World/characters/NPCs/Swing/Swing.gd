extends KinematicBody2D

var nearBoy = true

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _on_Area2D_body_entered(body):
	if(body.name == "Menino"):
		$ToBreakTimer.start()


func _on_ToBreakTimer_timeout():
	$Fixed.hide()
	$Broken.show()

func interact(item):
	if($Broken.is_visible_in_tree()):
		if(!nearBoy):
			get_node("/root/Main/HUD").add_item("rope")
			get_parent().add_child(preload("res://World/SuccessSFX.tscn").instance())
			$CollisionShape2D.disabled = true

func _on_Area2D_body_exited(body):
	if(body.name == "Menino"):
		nearBoy = false
