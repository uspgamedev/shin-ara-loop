extends KinematicBody2D

const EPS = 0.01

var speed = 23
var velocity = Vector2()
var lastDir = Vector2()
var flip = false

func _ready():
	pass

func _process(delta):
	var dir = Vector2(0, 0)
	if Input.is_action_pressed("ui_up"):
		dir.y -= speed
	if Input.is_action_pressed("ui_down"):
		dir.y += speed
	if Input.is_action_pressed("ui_left"):
		dir.x -= speed
		flip = true
	if Input.is_action_pressed("ui_right"):
		dir.x += speed
		flip = false
	if dir != Vector2(0,0):
		lastDir = dir
	if not dir and $Sprite/AnimationPlayer.current_animation != "idle":
		$Sprite/AnimationPlayer.play("idle")
	elif dir and $Sprite/AnimationPlayer.current_animation != "walk":
		$Sprite/AnimationPlayer.play("walk")
	$Sprite.flip_h = flip
	$InteractionArea/CollisionPolygon2D.rotation = Vector2(0,1).angle_to(lastDir)
	velocity += dir.normalized() * speed

func _physics_process(delta):
	move_and_slide(velocity)
	decelerate()

func _input(event):
	var hud = get_node("/root/Main/HUD")
	if event is InputEventKey and event.pressed:
		if event.is_action("interact"):
			var target = get_overlapping()
			if target != null:
				var item = hud.selected_item()
				var remove = target.interact(item)
				if remove:
					hud.remove_item(item.extern_name)
		elif event.is_action("next_item"):
			hud.next_item()

func get_overlapping():
	for body in $InteractionArea.get_overlapping_bodies():
		if body.has_method("interact"):
			return body
	return null

func decelerate():
	velocity *= .75
	if velocity.x < EPS and velocity.x > -EPS:
		velocity.x = 0
	if velocity.y < EPS and velocity.y > -EPS:
		velocity.y = 0
