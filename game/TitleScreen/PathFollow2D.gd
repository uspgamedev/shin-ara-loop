extends Node2D

var mult = 1
onready var cyclic = get_parent().cyclic
export (float) var speed = 1
export (bool) var override_parent_speed = false
var fg = true

func _ready():
	self.rotate = false
	if not override_parent_speed:
		speed = get_parent().speed

func _process(delta):
	var curdelta = delta*mult*speed
	if not self.cyclic and (curdelta + self.unit_offset > 1 or curdelta + self.unit_offset < 0):
		mult *= -1
		curdelta *= -1
	self.unit_offset += curdelta
	var decimal = self.unit_offset-floor(self.unit_offset)
	if decimal > 0.4 and fg:
		fg = false
		$Sprite.z_index = 0
	elif decimal > 0 and decimal < 0.4 and not fg:
		fg = true
		$Sprite.z_index = 1