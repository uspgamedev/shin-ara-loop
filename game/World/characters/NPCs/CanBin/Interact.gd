extends Control

func interact(item):
	if item != null:
		get_node("/root/Main/HUD/Bag").remove_item(item.extern_name)
		get_parent().get_node("SFX").play()