extends CanvasLayer

signal beginClockAnim
signal endClockAnim

func _ready():
	pass

func add_item(name):
	return $Bag.add_item(name)

func remove_item(name):
	$Bag.remove_item(name)

func selected_item():
	return $Bag.selected_item()

func next_item():
	$Bag.next_item()

func end_day(from_actual=false):
	var beg = 12 if not from_actual else $Clock.get_slided_frame()
	emit_signal("beginClockAnim")
	$Clock/Timer.stop()
	var tween = $Tween
	var initialPosition = $Clock/Sprite.position
	var finalPosition = Vector2(512, 300)
	var initialScale = $Clock/Sprite.scale
	var finalScale = Vector2(20/3, 20/3)
	var initialFrame = $Clock/Sprite.frame
	var ap = get_node("/root/Main/BGM")
	tween.interpolate_property(ap, "volume_db", ap.volume_db, -30, 4, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	tween.interpolate_property($Clock/Sprite, "scale", initialScale, finalScale, 4, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	tween.interpolate_property($Clock/Sprite, "position", initialPosition, finalPosition, 4, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	tween.start()
	yield(tween, "tween_completed")
	if not $Clock.fixed:
		var ret_time = 5.0*beg/12.0 if beg != 0 else 0.1
		tween.interpolate_method($Clock, "set_slided_frame", beg, 0, ret_time, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
		tween.start()
		yield(tween, "tween_completed")
		yield(tween, "tween_completed")
		tween.interpolate_property($Clock/Sprite, "scale", finalScale, initialScale, 4, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
		tween.interpolate_property($Clock/Sprite, "position", finalPosition, initialPosition, 4, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
		tween.start()
		yield(tween, "tween_completed")
		yield(tween, "tween_completed")
		emit_signal("endClockAnim")
		$Clock/Timer.start()
	else:
		tween.interpolate_property($Fade, "color", Color(0,0,0,0), Color(0,0,0,1), 6, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
		tween.start()
		for i in range(8):
			yield(get_tree().create_timer(1.0), "timeout")
			$Clock.set_slided_frame(i+1, true)
		get_tree().change_scene("res://TitleScreen/Menu.tscn")
