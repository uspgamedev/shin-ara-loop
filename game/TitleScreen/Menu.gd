extends Control

func _ready():
	$AudioStreamPlayer.play()
	var tween = Tween.new()
	tween.interpolate_property($AudioStreamPlayer,"volume_db", -20, 1, 1, Tween.EASE_IN, Tween.EASE_OUT)
	tween.start()
	self.add_child(tween)

func _input(event):
	if event.is_action_pressed("interact"):
		$TickSFX.play()
		$Play.shake()
		yield(get_tree().create_timer(1.0), "timeout")
		get_tree().change_scene("res://Main.tscn")