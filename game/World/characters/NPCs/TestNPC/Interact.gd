extends Control

func interact(item):
	if item != null and item.extern_name == "coins":
		print("Right")
		get_node("../Reaction").react("happy")
		return true
	print("Wrong")
	return false