extends Node2D

var instanced
const bug = preload("res://World/characters/NPCs/Bush/Bug/Bug.tscn")

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	spawnBug()
	$Destructor.start()
	pass

func spawnBug():
	instanced = bug.instance()
	var newPosition = Vector2(-29, -20)
	instanced.set_position(newPosition)
	add_child(instanced)

func _on_Generator_timeout():
	spawnBug()
	$Destructor.start()


func _on_Destructor_timeout():
	if(instanced != null):
		instanced.queue_free()
		


