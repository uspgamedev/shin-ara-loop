extends Node2D

func _ready():
	pass

func react(emotion, flip=false):
	set_reaction(emotion, flip)
	$Timer.start()

func set_reaction(emotion, flip=false):
	$Balloon.flip_h = flip
	$Balloon/Emotion.flip_h = flip
	$Balloon/Emotion.texture = load("res://World/reaction/emotions/" + emotion + ".png")
	$Balloon.visible = true

func clear_reaction():
	$Balloon.visible = false