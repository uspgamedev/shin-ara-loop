extends Control

func interact(item):
	var parent = get_node("..")
	if(item != null):
		if(item.extern_name == "rope"):
			if(!parent.blocked):
				parent.get_node("CollisionShape2D").disabled = true
				parent.get_node("WithRope").show()
				parent.get_node("WithoutRope").hide()
				return true