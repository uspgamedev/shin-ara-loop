extends Control

func interact(item):
	if(item != null):
		if(item.extern_name == "bug"):
			var bag = get_node("/root/Main/HUD/Bag")
			bag.remove_item("bug")
			yield(bag, "item_removed")
			bag.add_item("brokenNut")
			get_node("..").get_node("CollisionShape2D").disabled = true