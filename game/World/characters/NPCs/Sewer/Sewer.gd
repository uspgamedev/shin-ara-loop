extends KinematicBody2D

func interact(item):
	print("clicked")
	if item != null and item.extern_name == "fishingrod":
		get_node("/root/Main/HUD").remove_item("fishingrod")
		get_node("/root/Main/HUD").add_item("brokenNail")
		get_parent().add_child(preload("res://World/SuccessSFX.tscn").instance())
		self.queue_free()
	return false
