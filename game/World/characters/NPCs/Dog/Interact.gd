extends Control
const SFX = preload("res://World/SuccessSFX.tscn")
func interact(item):
	if item != null and item.extern_name == "bone":
		
		get_node("..").get_parent().add_child(SFX.instance())
		get_node("..").isFed = true
		get_node("../Reaction").react("love")
		return true
	get_node("../Reaction").react("quest")
	return false
