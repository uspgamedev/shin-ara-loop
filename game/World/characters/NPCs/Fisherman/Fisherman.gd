extends "res://World/characters/NPCs/NPCBase.gd"

export (NodePath) var destination

const ANIMATION_TIME = 7
const SFX = preload("res://World/SuccessSFX.tscn")
var hasFishingRod = true

func _ready():
	$Reaction.set_reaction("sad")

func moveToDestination():
	$Tween.interpolate_property(self, "position", position, get_node(destination).position,
							ANIMATION_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()
	$FishingRod.hide()

func giveFishingRod():
	get_node("..").get_parent().add_child(SFX.instance())
	get_node("/root/Main/HUD").add_item("fishingrod")
	moveToDestination()
	hasFishingRod = false
	$Reaction.react("happy")
	$GiveRodSFX.play()

func _on_Area2D_body_entered(body):
	if(body.name == "Player") and hasFishingRod:
		$Area2D/Timer.start()
		if hasFishingRod:
			$Reaction.set_reaction("happy")

func _on_Area2D_body_exited(body):
	if(body.name == "Player") and hasFishingRod:
		$Area2D/Timer.stop()
		if hasFishingRod:
			$Reaction.set_reaction("sad")

